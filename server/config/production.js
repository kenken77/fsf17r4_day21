'use strict';

module.exports = {
  PORT: process.env.PORT,

  SECRET: process.env.SESSION_SECRET,
  GooglePlus_key: process.env.GOOGLE_PLUS_KEY,
  GooglePlus_secret: process.env.GOOGLE_PLUS_SECRET,
  GooglePlus_callback_url: process.env.GOOGLE_PLUS_CALLBACK_URL,
  Facebook_key: process.env.FACEBOOK_KEY,
  Facebook_secret: process.env.FACEBOOK_SECRET,
  Facebook_callback_url: process.env.FACEBOOK_CALLBACK_URL,
  
  USER_DATABASE: [
    {
      username: 'adam',
      password: 'mada',
    },
    {
      username: 'betty',
      password: 'ytteb',
    },
    {
      username: 'charles',
      password: 'selrahc',
    },
    {
      username: 'denise',
      password: 'esined',
    },
    {
      username: 'eric',
      password: 'cire',
    }
  ],

  version: '1.0',
};