require('dotenv').config();
var express = require('express');
var app = express();
var path = require('path');
var config = require('./config');
var session = require('express-session');
var passport = require('passport');
var flash = require('connect-flash');

var bodyParser = require('body-parser');
var PORT = config.PORT;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

console.log(config.SECRET);
app.use(session({
  secret: config.SECRET,
  resave: true,
  saveUninitialized: true,
}));

app.use(flash());
app.use(passport.initialize());
app.use(passport.session());

app.use(express.static(path.join(__dirname, '/../client')));

var auth = require('./auth')(app, passport);
require('./routes')(auth, app, passport);

app.listen(PORT, function() {
  console.log("Server started on port %d...", PORT);
});